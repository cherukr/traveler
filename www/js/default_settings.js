/*
 * This module defines the default settings for the application. They are loaded on bootup if there are no settings stored on the LocalStorage.
 */
angular.module('traveler.default_settings', [])
        .constant("default_settings", {
            mapCenter: {
                lat: 50,
                lng: 7
            },
            zoom: 12,
            homePage: "app.map",
            homeView: "all",
            closeRange: 850,
            stateFilters: [
                {text: 'Created', name: 'created', checked: true},
                {text: 'Assigned', name: 'assigned', checked: true},
                {text: 'Acknowledged', name: 'acknowledged', checked: true},
                {text: 'In Progress', name: 'in_progress', checked: true},
                {text: 'Rejected', name: 'rejected', checked: true},
                {text: 'Solved', name: 'solved', checked: true}
            ]
        });