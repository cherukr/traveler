/*
 * This module defines the messages and their value that can be showned to the user while using the application.
 */
angular.module('traveler.messages', [])
        .constant("messages", {
            no_result: "Sorry, no results ...",
            connecting: "Connecting...",
            error: "Could not load issues.",
            error_issue: "Could not load issues",
            loading: "Loading...",
            locating: "Locating",
            load_map: "Loading Map",
            load_picture: "Loading Photo",
            load_data: "Loading Data",
            create_markers: "Creating Markers",
            filter_fail: "No results for these filters"
        });